This project is published in the hope that it will be interesting to
those who find it, but at present not in the hope that it will be
widely used.

To that end I am not soliciting contributions and users at the present
time. Any attempts to contribute to this software (by people I have
not explicitly requested feedback from), be it by filing bugs,
submitting patches, writing documentation, etc will be ignored or
result in relentless mocking.
