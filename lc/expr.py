from collections import Counter
from decimal import Decimal as d
import functools
import io

from . import convert
from . import parse
from . import units
from . import unit_definitions


class Value:
    def __init__(self, value, unit=None, tokens=None):
        self.tokens = (tokens or [])
        self.value = value
        self.unit = unit

    def evaluate(self): return self

    def output_form(self):
        if self.unit:
            if self.value == 1:
                return f"{self.value} {self.unit.name}"
            else:
                return f"{self.value} {units.pluralize(self.unit.name)}"
        else:
            return f"{self.value}"

    def __repr__(self):
        return f"Value({self.value}, {self.unit!r})"

    def __mul__(self, other):
        if self.unit and other.unit:
            raise NotImplementedError('Multiplying two values with units is not yet implemented')
        elif self.unit and not other.unit:
            return Value(self.value * other.value, self.unit)
        elif other.unit and not self.unit:
            return Value(self.value * other.value, other.unit)
        else:
            return Value(self.value * other.value)

    def __truediv__(self, other):
        if self.unit and other.unit:
            raise NotImplementedError('Dividing two values with units is not yet implemented')
        elif self.unit and not other.unit:
            return Value(self.value / other.value, self.unit)
        elif other.unit and not self.unit:
            return Value(self.value / other.value, other.unit)
        else:
            return Value(self.value / other.value)

    def __add__(self, other):
        if isinstance(other, Value):
            if not other.unit:
                return Value(self.value + other.value, self.unit)
            elif not self.unit:
                return Value(self.value + other.value, other.unit)
            elif other.unit != self.unit:
                # convert to the more precise unit
                # todo: maybe the one that's closest to the primitive instead?
                other_converted = other.convert_unit(self.unit)
                self_converted = self.convert_unit(other.unit)
                if self_converted.value > self.value:
                    return Value(self_converted.value + other.value, other.unit)
                else:
                    return Value(other_converted.value + self.value, self.unit)
            else:
                return Value(self.value + other.value, self.unit)
        else:
            return self.__add__(Value(other))

    def __sub__(self, other):
        if isinstance(other, Value):
            return self.__add__(Value(-other.value, other.unit))
        else:
            return self.__add__(-other.value)

    def convert_unit(self, to_unit):
        if not self.unit:
            return Value(self.value, to_unit, tokens=self.tokens)

        path = convert.find_conversion_path(self.unit.name, to_unit.name)
        if not path: raise UnitConversionImpossible(f"Can't convert from {self.unit!r} to {to_unit!r}")
        val = self.value
        for ifrom, ito in convert.pairwise(path): # 'intermediate from'/'... to'
            # print(f"{ifrom} -> {ito}")
            if ifrom in unit_definitions.units and unit_definitions.units[ifrom][1] == ito:
                val *= unit_definitions.units[ifrom][0]
            else:
                val /= unit_definitions.units[ito][0]

        return Value(val, to_unit, tokens=self.tokens)

@functools.total_ordering
class Unit:
    def __init__(self, name):
        if isinstance(name, Unit):
            # protect against accidentally calling Unit on a Unit
            self.name = name.name
        else:
            self.name = name

    def output_form(self):
        # usually when we're outputting a unit name alone, we want it in the plural
        return units.pluralize(self.name)

    def __mul__(self, other):
        if isinstance(other, CompoundUnit):
            return other.__mul__(self)
        elif isinstance(other, Unit):
            if other == self:
                return CompoundUnit([(self, 2)])
            else:
                return CompoundUnit([(self, 1), (other, 1)])
        else:
            raise NotImplementedError("tried to multiply a unit by something oher than a unit, and I'm not sure this makes sense")

    def __truediv__(self, other):
        if isinstance(other, CompoundUnit):
            return CompoundUnit([(self, 1), *((basic_unit, -power) for basic_unit, power in other.components.items())])
        elif isinstance(other, Unit):
            if other == self:
                return CompoundUnit([(self, -1)])
            else:
                return CompoundUnit([(self, 1), (other, -1)])
        else:
            raise NotImplementedError("tried to divide a unit by something oher than a unit, and I'm not sure this makes sense")

    def __eq__(self, other): return self.name == other.name
    def __lt__(self, other): return self.name < other.name

    def __hash__(self): return hash(self.name) ^ hash(Unit)
    def __repr__(self):
        return f"Unit({self.name!r})"

class CompoundUnit:
    def __init__(self, components=None):
        self.components = Counter({Unit(basic_unit): power for basic_unit, power in (components or []) if power != 0})

    def __mul__(self, other):
        new_components = self.components.copy()
        if isinstance(other, Unit):
            new_components[other] += 1
        elif isinstance(other, CompoundUnit):
            for basic_unit, power in other.components.items():
                new_component[basic_unit] += power

        return CompoundUnit(new_components.items())

    def __truediv__(self, other):
        new_components = self.components.copy()
        if isinstance(other, Unit):
            new_components[other] -= 1
        elif isinstance(other, CompoundUnit):
            for basic_unit, power in other.components.items():
                new_component[basic_unit] -= power

        return CompoundUnit(new_components.items())

    def __hash__(self):
        return hash(tuple(self.components.most_common())) ^ hash(CompoundUnit)
    def __eq__(self, other):
        return self.components.most_common() == other.components.most_common()
    def __repr__(self):
        return f"CompoundUnit({self.components.most_common()!r})"

precedence = {op[0]: precedence for precedence, op in enumerate(parse.binary_operators)}
class BinaryOperation:
    def __init__(self, operator, a, b):
        self.operator = operator
        self.a = a
        self.b = b

    def evaluate(self):
        op = self.operator
        if op == '/':
            return self.a.evaluate() / self.b.evaluate()
        elif op == '*':
            return self.a.evaluate() * self.b.evaluate()
        elif op == '+':
            return self.a.evaluate() + self.b.evaluate()
        elif op == '-':
            return self.a.evaluate() - self.b.evaluate()
        elif op == 'in':
            if isinstance(self.a, Unit):
                return Value(d(1), self.a).convert_unit(self.b)
            else:
                return self.a.evaluate().convert_unit(self.b)
        else:
            raise NotImplementedError(f"The {op!r} operator is not implemented yet")

    def output_form(self):
        out = io.StringIO()
        if isinstance(self.a, BinaryOperation) and precedence[self.a.operator] > precedence[self.operator]:
            out.write('(')
            out.write(self.a.output_form())
            out.write(')')
        else:
            out.write(self.a.output_form())

        out.write(f" {self.operator} ")

        if isinstance(self.b, BinaryOperation) and precedence[self.b.operator] > precedence[self.operator]:
            out.write('(')
            out.write(self.b.output_form())
            out.write(')')
        else:
            out.write(self.b.output_form())

        out.seek(0)
        return out.read()

    def __repr__(self):
        return f"BinaryOperation({self.operator!r}, {self.a!r}, {self.b!r})"
