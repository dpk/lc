from collections import defaultdict

import regex
from requests.structures import CaseInsensitiveDict

from . import unit_definitions


unit_normalizations = CaseInsensitiveDict()
for unit in unit_definitions.units.keys():
    unit_normalizations[unit] = unit
for unit_alias, unit_real_name in unit_definitions.unit_aliases.items():
    unit_normalizations[unit_alias] = unit_real_name
for singular, weird_plural in unit_definitions.weird_plurals.items():
    unit_normalizations[weird_plural] = singular

es_plural_ending_re = regex.compile(r"\L<endings>$", flags=regex.V1, endings=['s', 'sh', 'ch'])
ies_plural_ending_re = regex.compile(r"[^aeiou]y$", flags=regex.V1)
def basic_pluralize(name):
    if es_plural_ending_re.search(name):
        return f"{name}es"
    elif ies_plural_ending_re.search(name):
        return f"{name[:-1]}ies"
    else:
        return f"{name}s"
def pluralize(name, number=2):
    if number == 1: return name
    if name in unit_definitions.weird_plurals:
        return unit_definitions.weird_plurals[name]
    else:
        return basic_pluralize(name)

for singular in unit_definitions.primitive_units:
    unit_normalizations[singular] = singular
    unit_normalizations[basic_pluralize(singular)] = unit_normalizations[singular]
for singular in unit_definitions.units.keys():
    unit_normalizations[basic_pluralize(singular)] = unit_normalizations[singular]
for singular in unit_definitions.unit_aliases.keys():
    unit_normalizations[basic_pluralize(singular)] = unit_normalizations[singular]

reverse_conversions = defaultdict(set)
for unit, value in unit_definitions.units.items():
    reverse_conversions[value[1]].add(unit)

def normalize_unit(name):
    # we try case-sensitive matches first so we correctly handle M for mega and m for milli
    if name in unit_definitions.units:
        return name
    elif name in unit_definitions.unit_abbreviations:
        return normalize_unit(unit_definitions.unit_abbreviations[name])
    elif name in unit_definitions.weird_plurals:
        return unit_definitions.weird_plurals[name]
    elif name in unit_definitions.unit_aliases:
        return unit_definitions.unit_aliases[name]
    elif name in unit_normalizations:
        return unit_normalizations[name]
    else:
        assert False, f"what is this strange unit: {name!r}"
