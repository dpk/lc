from decimal import Decimal as d


pi = d('3.1415926535897932384626433832795028841971693993751058209749445923078164')

primitive_units = {
    # SI primitives
    'ampere',
    'candela',
    'gram', # SI says kilogram but if we do that, that makes si_full_prefix not work
    'metre',
    'mole',
    'second',

    # non-SI primitives (dimensionless)
    'radian',
    'bit',
    'musical cent',
}

units = {
    # metric units of distance are handled by SI prefix generation
    # imperial units of distance
    'inch': (d('25.4'), 'millimetre'), # 'Anglo-Saxon compromise' inch of 1959
    'thousandth of an inch': (d('0.001'), 'inch'),
    'foot': (12, 'inch'),
    'yard': (3, 'foot'),
    'mile': (1760, 'yard'),

    # slightly more archaic imperial lengths
    'barleycorn': (d(1) / 3, 'inch'),
    'chain': (22, 'yard'),
    'furlong': (10, 'chain'),

    # printing units of length
    'PostScript point': (d('1') / 72, 'inch'),
    'TeX point': (1 / d('72.27'), 'inch'),

    # we're in space
    'astronomical unit': (149597870700, 'metre'),
    'parsec': (648000/pi, 'astronomical unit'), # 2015 redefinition
    'light year': (9460730472580800, 'metre'),

    # some esoteric units of length
    'marathon': (d('42.195'), 'kilometre'),

    # metric units of weight are mostly handled by SI prefix generation, except:
    'tonne': (1000, 'kilogram'),
    # imperial units of weight
    'ounce': (d('28.349523125'), 'gram'),
    'pound': (16, 'ounce'),
    'stone': (14, 'pound'),
    'hundredweight': (112, 'pound'),
    'US hundredweight': (100, 'pound'),
    'long ton': (20, 'hundredweight'),
    'short ton': (2000, 'pound'),

    # standard units of time
    'minute': (60, 'second'),
    'hour': (60, 'minute'),
    'SI day': (24, 'hour'),
    'mean solar day': (d('86400.002'), 'second'),
    'week': (7, 'SI day'),
    'mean solar year': (d('365.24219'), 'SI day'),
    'mean Gregorian year': (d('365.2425'), 'SI day'),
    'twelfth of a Gregorian year': (d(1)/12, 'mean Gregorian year'),

    # some esoteric units of time
    'fortnight': (2, 'week'),
    'microfortnight': (d(1)/1000000, 'fortnight'),

    # units of angle
    'degree': (pi / 180, 'radian'),
    'gradian': (d('100') / 90, 'degree'),
    'arcminute': (d('1') / 60, 'degree'),
    'arcsecond': (d('1') / 60, 'arcminute'),
    'turn': (2 * pi, 'radian'),
    'right angle': (90, 'degree'),

    # units of information
    'byte': (8, 'bit'),
    'nybble': (4, 'bit'),

    'decimal kilobyte': (1000, 'byte'),
    'decimal megabyte': (1000, 'decimal kilobyte'),
    'decimal gigabyte': (1000, 'decimal megabyte'),
    'decimal terabyte': (1000, 'decimal gigabyte'),
    'decimal petabyte': (1000, 'decimal terabyte'),
    'decimal exabyte': (1000, 'decimal petabyte'),
    'decimal zettabyte': (1000, 'decimal exabyte'),
    'decimal yottabyte': (1000, 'decimal zettabyte'),

    'kibibyte': (1024, 'byte'),
    'mebibyte': (1024, 'kibibyte'),
    'gibibyte': (1024, 'mebibyte'),
    'tebibyte': (1024, 'gibibyte'),
    'pebibyte': (1024, 'tebibyte'),
    'exbibyte': (1024, 'pebibyte'),
    'zebibyte': (1024, 'exbibyte'),
    'yobibyte': (1024, 'zebibyte'),

    # musical intervals
    'octave': (1200, 'musical cent'),
    'equal-tempered minor third': (300, 'musical cent'),
    'perfect minor third': (d('315.641'), 'musical cent'),
    'perfect fourth': (d('498.045'), 'musical cent'),
    'equal-tempered fourth': (d('498.045'), 'musical cent'),
    'wolf fourth': (d('519.551'), 'musical cent'),
    'equal-tempered major third': (400, 'musical cent'),
    'wolf fifth': (d('680.449'), 'musical cent'),
    'equal-tempered fifth': (700, 'musical cent'),
    'perfect fifth': (d('701.955'), 'musical cent'),
    'equal-tempered semitone': (100, 'musical cent'),
}
weird_plurals = {
    'foot': 'feet',
    'thousandth of an inch': 'thousandths of an inch',
    'hundredweight': 'hundredweight',
    'US hundredweight': 'US hundredweight',
    'twelfth of a Gregorian year': 'twelfths of a Gregorian year',
}

unit_aliases = {
    'cent': 'musical cent',
    'day': 'SI day',
    'Gregorian year': 'mean Gregorian year',
    'lightyear': 'light year',
    'meter': 'metre',
    'month': 'twelfth of a Gregorian year',
    'octet': 'byte',
    'point': 'PostScript point',
    'quadrant': 'right angle',
    'semitone': 'equal-tempered semitone',
    'ton': 'tonne',
    'year': 'mean solar year',

    'kilobyte': 'decimal kilobyte',
    'megabyte': 'decimal megabyte',
    'gigabyte': 'decimal gigabyte',
    'terabyte': 'decimal terabyte',
    'petabyte': 'decimal petabyte',
    'exabyte': 'decimal exabyte',
    'zettabyte': 'decimal zettabyte',
    'yottabyte': 'decimal yottabyte',

    'binary kilobyte': 'kibibyte',
    'binary megabyte': 'mebibyte',
    'binary gigabyte': 'gibibyte',
    'binary terabyte': 'tebibyte',
    'binary petabyte': 'pebibyte',
    'binary exabyte': 'exbibyte',
    'binary zettabyte': 'zebibyte',
    'binary yottabyte': 'yobibyte',    
}
unit_abbreviations = {
    'A': 'ampere',
    'am': 'arcminute',
    'arcmin': 'arcminute',
    'arcsec': 'arcsecond',
    'as': 'arcsecond',
    'au': 'astronomical unit',
    'cd': 'candela',
    'cwt': 'hundredweight',
    'deg': 'degree',
    '°': 'degree',
    'ft': 'foot',
    'g': 'gram',
    'grad': 'gradian',
    #'in': 'inch', # fixme
    'lb': 'pound',
    'ly': 'light year',
    'm': 'metre',
    'mi': 'mile',
    'min': 'minute',
    'mol': 'mole',
    'oz': 'ounce',
    'pc': 'parsec',
    'pt': 'PostScript point',
    'rad': 'radian',
    's': 'second',
    'st': 'stone',
    'thou': 'thousandth of an inch',
    'ua': 'astronomical unit',
    'yd': 'yard',
    'ch': 'chain',

    'B': 'byte',
    'kB': 'decimal kilobyte',
    'MB': 'decimal megabyte',
    'GB': 'decimal gigabyte',
    'TB': 'decimal terabyte',
    'PB': 'decimal petabyte',
    'EB': 'decimal exabyte',
    'ZB': 'decimal zettabyte',
    'YB': 'decimal yottabyte',

    'KiB': 'kibibyte',
    'MiB': 'mebibyte',
    'GiB': 'gibibyte',
    'TiB': 'tebibyte',
    'PiB': 'pebibyte',
    'EiB': 'exbibyte',
    'ZiB': 'zebibyte',
    'YiB': 'yobibyte',
}


def si_full_prefixes(unit):
    prefixes = {
        'yotta': 1000000000000000000000000,
        'zetta': 1000000000000000000000,
        'exa':   1000000000000000000,
        'peta':  1000000000000000,
        'tera':  1000000000000,
        'giga':  1000000000,
        'mega':  1000000,
        'kilo':  1000,
        'hecto': 100,
        'deca':  10,
        'deka':  10,
        'deci':  d(1)/10,
        'centi': d(1)/100,
        'milli': d(1)/1000,
        'micro': d(1)/1000000,
        'nano':  d(1)/1000000000,
        'pico':  d(1)/1000000000000,
        'femto': d(1)/1000000000000000,
        'atto':  d(1)/1000000000000000000,
        'zepto': d(1)/1000000000000000000000,
        'yocto': d(1)/1000000000000000000000000,
    }
    for prefix, value in prefixes.items():
        if unit in units or unit in primitive_units:
            units[f"{prefix}{unit}"] = (value, unit)
        elif unit in unit_aliases:
            units[f"{prefix}{unit}"] = (value, unit_aliases[unit])
        else:
            assert f"no unit {unit!r}"

def si_abbreviated_prefixes(unit):
    prefixes = {
        'Y': 'yotta',
        'Z': 'zetta',
        'E': 'exa',
        'P': 'peta',
        'T': 'tera',
        'G': 'giga',
        'M': 'mega',
        'k': 'kilo',
        'h': 'hecto',
        'da': 'deca',
        'd': 'deci',
        'c': 'centi',
        'm': 'milli',
        '\u00B5': 'micro', # MICRO SIGN
        '\u03BC': 'micro', # GREEK SMALL LETTER MU (technically incorrect, but will probably show up in real input)
        'u': 'micro', # ASCIIfied for convenience
        'n': 'nano',
        'p': 'pico',
        'f': 'femto',
        'a': 'atto',
        'z': 'zepto',
        'y': 'yocto',
    }
    full_name = unit_abbreviations[unit]
    for prefix, value in prefixes.items():
        unit_abbreviations[f"{prefix}{unit}"] = f"{value}{full_name}"

for unit in ['ampere', 'candela', 'gram', 'metre', 'meter', 'mole', 'second', 'arcsecond']:
    si_full_prefixes(unit)
for unit in ['A', 'cd', 'g', 'm', 'mol', 's', 'as']:
    si_abbreviated_prefixes(unit)
