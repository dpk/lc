from collections import namedtuple
from decimal import Decimal as d

import regex
from requests.structures import CaseInsensitiveDict

from . import errors
from . import expr
from . import units
from . import unit_definitions


class CalculatorParseError(errors.CalculatorError): pass

# operators in order of precedence -- tuples are aliases (normalized by the parser to the first one)
binary_operators = [
    ('in', 'to', 'into'),
    ('**', '^'),
    ('%', 'rem', 'mod'),
    ('/', '÷', 'per'),
    ('*', '×'),
	('+',),
    ('-',),
    ('<<',),
    ('>>',),
    ('band',),
    ('bor',),
    ('xor',),
]

right_associatives = {'**'}

token_re_source = r"""\G(?i:
    (?P<open_paren>\() |
    (?P<close_paren>\)) |
    (?P<unit>(?:\L<units>|(?-i:\L<unit_abbreviations>))\b) |
    (?P<binary_operator>\L<binary_operators>) |
    (?P<fraction>[1-9][0-9]*/[1-9][0-9]*) |
    (?P<number>(?:[1-9][0-9]*|[0-9])(?:\.[0-9]+)?) |
    (?P<whitespace>\s+)
)"""
token_re = regex.compile(
    token_re_source,
    flags=regex.V1 | regex.VERBOSE,
    binary_operators=[item for sublist in binary_operators for item in sublist],
    units=[key.casefold() for key in units.unit_normalizations.keys()],
    unit_abbreviations=unit_definitions.unit_abbreviations.keys()
)


def parse_input(expr_str):
    tokens = [token for token in tokenize_input(expr_str) if token.type != 'whitespace']
    syntax_items = tokens_to_syntax_items(tokens)
    return operator_precedence(syntax_items)

Token = namedtuple('Token', ['type', 'text', 'start_pos', 'end_pos'])
def tokenize_input(expr_str):
    last_end = 0
    for m in token_re.finditer(expr_str):
        last_end = m.end()
        for name, value in m.groupdict().items():
            if value:
                yield Token(name, value, m.start(), m.end())

    if last_end != len(expr_str):
        raise CalculatorParseError(f"Unknown syntax at position {last_end}")

class ParenExpr:
    def __init__(self, syntax_items=None, start_pos=None, end_pos=None):
        if syntax_items:
            self.syntax_items = syntax_items
        else:
            self.syntax_items = []

        self.start_pos = start_pos
        self.end_pos = end_pos

    def __repr__(self):
        return f"ParenExpr({self.syntax_items!r})"

class BinaryOperator:
    def __init__(self, op):
        self.op = op

    def __eq__(self, other):
        return isinstance(other, BinaryOperator) and self.op == other.op
    def __repr__(self):
        return f"BinaryOperator({self.op!r})"

def tokens_to_syntax_items(tokens):
    stack = [ParenExpr()]
    def last_syntax_item():
        if len(stack[-1].syntax_items) > 0:
            return stack[-1].syntax_items[-1]
    def push(item): stack[-1].syntax_items.append(item)
    def pop(): return stack[-1].syntax_items.pop()

    for token in tokens:
        lsi = last_syntax_item()
        if token.type == 'number':
            push(expr.Value(d(token.text), tokens=[token]))
        elif token.type == 'fraction':
            numerator, denominator = (d(x) for x in token.text.split('/'))
            push(Value(numerator / denominator, tokens=[token]))
        elif token.type == 'unit':
            unit = expr.Unit(units.normalize_unit(token.text))

            # todo: support compound units here
            if isinstance(lsi, expr.Value) and lsi.unit == None:
                lsi.unit = unit
                lsi.tokens.append(token)
            elif isinstance(lsi, ParenExpr):
                pop()
                push(ParenExpr([lsi, BinaryOperator('in'), unit]))
            else:
                push(unit)
        elif token.type == 'open_paren':
            stack.append(ParenExpr(start_pos=token.start_pos))
        elif token.type == 'close_paren':
            paren_expr = stack.pop()
            if len(stack) == 0:
                raise CalculatorParseError(f'Attempted to close a parenthesis that was never opened at position {token.start_pos}')
            paren_expr.end_pos = token.start_pos
            push(paren_expr)
        elif token.type == 'binary_operator':
            op = BinaryOperator(normalize_operator(token.text))
            push(op)
        else:
            raise CalculatorParseError(f'Unexpected token {token.type!r} a position {token.start_pos}')

    if len(stack) > 1:
        raise CalculatorParseError(f'Opened a parenthesis that never gets closed at position {stack[1].start_pos}') # @@ slightly wrong

    return stack[0].syntax_items

operator_normalizations = CaseInsensitiveDict()
for operator in binary_operators:
    for op_sign in operator:
        operator_normalizations[op_sign] = operator[0]
def normalize_operator(op):
    return operator_normalizations[op]

# this is a ridiculously inefficient method
def operator_precedence(syntax_items):
    if len(syntax_items) == 1:
        if isinstance(syntax_items[0], ParenExpr):
            return operator_precedence(syntax_items[0].syntax_items)
        else:
            return syntax_items[0]

    for operator in reversed(binary_operators):
        lhs = []
        operator = operator[0]
        if operator in right_associatives:
            items = syntax_items
        else:
            items = reversed(syntax_items)

        for idx, item in enumerate(items):
            if isinstance(item, BinaryOperator) and item.op == operator:
                if operator in right_associatives:
                    rhs = items[idx + 1:]
                else:
                    rhs = syntax_items[:-(idx+1)]
                    lhs, rhs = rhs, list(reversed(lhs))

                if len(lhs) == 0:
                    # fixme: could be a unary operator
                    raise CalculatorParseError(f'Operator {item.op!r} seemingly has no left-hand operand at position {item.start_pos}')
                elif len(rhs) == 0:
                    raise CalculatorParseError(f'Operator {item.op!r} seemingly has no right-hand operand at position {item.start_pos}')

                return expr.BinaryOperation(item.op, operator_precedence(lhs), operator_precedence(rhs))
            elif isinstance(item, ParenExpr):
                size = len(item.syntax_items)
                if size == 0:
                    raise CalculatorParseError(f'Empty parenthetical expression at position {item.start_pos}')
                elif size == 1:
                    lhs.append(item.syntax_items[0])
                else:
                    lhs.append(operator_precedence(item.syntax_items))
            else:
                lhs.append(item)
