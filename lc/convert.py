import itertools

from . import errors
from . import unit_definitions
from . import units


class UnitConversionImpossible(errors.CalculatorError): pass

# simple depth-first search -- might make sense to switch to Dijkstra in the future?
def find_conversion_path(from_unit, to_unit):
    if from_unit == to_unit: return [from_unit]
    visited = set()
    def _find_conversion_path(_from_unit, stack):
        visited.add(_from_unit)
        to_visit = units.reverse_conversions.get(_from_unit, set())
        if _from_unit in unit_definitions.units:
            to_visit.add(unit_definitions.units[_from_unit][1])

        if to_unit in to_visit:
            return stack + [to_unit]
        for node_to_visit in to_visit:
            if node_to_visit not in visited:
                result = _find_conversion_path(node_to_visit, stack + [node_to_visit])
                if result: return result

    return _find_conversion_path(from_unit, [from_unit])

def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)

